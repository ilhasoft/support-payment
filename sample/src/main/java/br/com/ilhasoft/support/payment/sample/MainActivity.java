package br.com.ilhasoft.support.payment.sample;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import br.com.ilhasoft.support.payment.billing.IabHelper;
import br.com.ilhasoft.support.payment.billing.IabResult;
import br.com.ilhasoft.support.payment.billing.Purchase;
import br.com.ilhasoft.support.payment.sample.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    public static final int REQUEST_CODE_SUBSCRIPTION = 405;
    public static final String SKU_SUBSCRIPTION = "example_sku";

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSubscription();
            }
        });
    }

    private void startSubscription() {
        final IabHelper iabHelper = new IabHelper(this, "base64here");
        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                appendResultToLog(result);
                Log.d(TAG, "onIabSetupFinished() called with: result = [" + result + "]");
                try {
                    iabHelper.launchSubscriptionPurchaseFlow(MainActivity.this, SKU_SUBSCRIPTION
                            , REQUEST_CODE_SUBSCRIPTION, new IabHelper.OnIabPurchaseFinishedListener() {
                                @Override
                                public void onIabPurchaseFinished(IabResult result, Purchase info) {
                                    Log.d(TAG, "onIabPurchaseFinished() called with: result = [" + result + "], info = [" + info + "]");

                                    appendResultToLog(result);
                                    binding.log.append("Order ID: " + info.getOrderId());
                                    binding.log.append("\n"); binding.log.append("\n");
                                    binding.log.append("Purchase Time: " + info.getPurchaseTime());
                                    binding.log.append("\n"); binding.log.append("\n");
                                    binding.log.append("Original JSON: " + info.getOriginalJson());
                                    binding.log.append("\n");
                                }
                            });
                } catch(Exception exception) {
                    Log.e(TAG, "launchSubscriptionInitialPlan: ", exception);
                }
            }
        });
    }

    private void appendResultToLog(IabResult result) {
        binding.log.append(result.getResponse() + " - " + result.getResponse());
        binding.log.append("\n"); binding.log.append("\n");
    }
}
